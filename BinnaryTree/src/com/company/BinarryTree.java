package com.company;

public class BinarryTree {
    Node root;

    BinarryTree() {
        root = null;
    }

    void Inorder(Node node) {
        /* Inorder traversal
        will visit the left subtree then the root then the right subtree
         */
        if (node == null)
            return;

        Inorder(node.left); // to visit the lift subtree
        System.out.print(node.item + "->"); // to visit the root
        Inorder(node.right);// to visit the right
    }

    void Preorder(Node node) {
        /* Preorder traversal
        will visit the root then the left subtree then the right subtree
        */
        if (node == null)
            return;

        System.out.print(node.item + "->"); // to visit the root
        Preorder(node.left); // to visit the left subtree
        Preorder(node.right); // to visit the right subtree
    }

    void Postorder(Node node) {
        /*Postorder traversal
        will visit the lift subtree then the right subtree then the root
        */
        if (node == null)
            return;

        Postorder(node.left); // to visit the left subtree
        Postorder(node.right); // to visit the right  subtree
        System.out.print(node.item+"->"); // to visit the root
    }

}
