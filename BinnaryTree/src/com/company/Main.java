package com.company;

public class Main {

    public static void main(String[] args) {
        BinarryTree tree = new BinarryTree();
        tree.root = new Node(1);
        tree.root.left = new Node(12);
        tree.root.right = new Node(9);
        tree.root.left.left = new Node(5);
        tree.root.left.right = new Node(6);

        System.out.println("The Inorder traversal");
        tree.Inorder(tree.root);
        System.out.println("\nThe Preorder traversal");
        tree.Preorder(tree.root);
        System.out.println("\nThe Postorder traversal");
        tree.Postorder(tree.root);


    }
}
