package com.company;

public class CheckElement {
    public static boolean checkValues(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    return true;
                }

            }

        }
        return false;
    }

    public static void main(String[] args) {
        int[] arr = {50, 60, 100, 500, 4, 100};
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }

        if (checkValues(arr)) {
            System.out.println("\n The two numbers is found ");
        } else
            System.out.println("The two numbers is not found");
    }
}


