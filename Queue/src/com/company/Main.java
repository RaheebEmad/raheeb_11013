package com.company;

public class Main {

    public static void main(String[] args) {
  QueueArray q=new QueueArray(6);
        q.enqueue(5);
        q.enqueue(4);
        q.enqueue(3);
        q.enqueue(2);
        q.enqueue(1);
        System.out.println("......The queue before deleting......:");
        q.display();
        q.TheFront();
        q.TheRear();
        System.out.println("\n......The queue after deleting......:");
        q.dequeue();
        q.dequeue();
        q.display();
        q.TheFront();
        q.TheRear();
    }
}
