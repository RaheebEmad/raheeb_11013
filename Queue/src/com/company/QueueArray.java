package com.company;

public class QueueArray {
    int size;
    int[] array;
    int front , rear;

    public QueueArray(int size) {
        this.size = size;
        array=new int[this.size];
        front=-1;
        rear=-1;
    }

    public boolean isFull(){
        return (front==0 && rear==size-1);
    }

    public boolean isEmpty(){
        return (front==-1);
    }

    public void enqueue(int item){
        if (isFull()){
            System.out.println("The queue is full");
        }
        else {
            front = 0;
            rear++;
            array[rear] = item;
        }
    }

    public void dequeue(){
        if (isEmpty()){
            System.out.println("The queue is empty");
        }
        else {
            if (front >= rear) {
                front = -1;
                rear=-1;
            }
            else
                front=front+1;

        }
    }


    public void display() {
        if (isEmpty()) {
            System.out.println("The queue is empty");
        }
        else
            for (int i = front; i <=rear;i++){
                System.out.print(array[i] + "\t");
            }

    }
    public void TheFront(){
        if (isEmpty()){
            System.out.println("The top of the queue is :-1");
        }
        else
            System.out.print("\nThe Front of the queue is :"+array[front]);
    }

    public void TheRear(){
        if (isEmpty()){
            System.out.print("\nThe rear of the queue is :-1");
        }
        else
            System.out.println("\nThe rear of the queue is :"+array[rear]);
    }

}
