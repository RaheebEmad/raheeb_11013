package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first salary :");
        int firstSalary = scanner.nextInt();
        System.out.println("Please enter the second salary :");
        int secondSalary = scanner.nextInt();
        System.out.println("Please enter the third salary :");
        int thirdSalary = scanner.nextInt();
        int largest = theLargest(firstSalary, secondSalary, thirdSalary);
        int smallest = theSmallest(firstSalary, secondSalary, thirdSalary);
        int diff=largest-smallest;
        System.out.println("The difference between the the biggest salary and the smallest salary is "+diff);

    }
    public static int theLargest(int first,int second,int third){
        int max=first;
        if (second > max) {
            max = second;
        }
        if (third > max) {
            max = third;
        }
        return max;
    }

    public static int theSmallest(int first,int second,int third){
        int  min=first;
        if (second < min) {
            min = second;
        }
        if (third < min) {
            min = third;
        }
        return min;
    }

}
