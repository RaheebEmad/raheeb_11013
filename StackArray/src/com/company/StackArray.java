package com.company;

public class StackArray {
    int size;
    int[] array;
    int bottom, top;

    public StackArray(int size) {
        this.size = size;
        array = new int[this.size];
        bottom = -1;
        top = -1;
    }

    public boolean isFull() {
        return (top == size - 1);
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public void push(int item) {
        if (isFull()) {
            System.out.println("The stack is full ");
        } else {
            bottom = 0;
            top++;
            array[top] = item;
        }
    }

    public void pop() {
        if (isEmpty()) {
            System.out.println("The stack is empty ");
        } else {
            if (top == bottom) {
                top = bottom = -1;
            } else {
                top--;
            }

        }
    }

    public void display() {
        if (isEmpty()){
            System.out.println("The stack is empty");
        }
        else
        for (int i = top; i >= bottom; i--) {
            System.out.println(array[i]);
        }


    }
    public void TheTop(){
        if (isEmpty()){
            System.out.println("The top of the stack is :-1");
        }
        else
        System.out.println("The top of the stack is :"+array[top]);
    }
    public void TheBottom(){
    if (isEmpty()){
        System.out.println("The bottom of the stack is :-1");
    }
    else
        System.out.println("The bottom of the stack is :"+array[bottom]);
    }
}
