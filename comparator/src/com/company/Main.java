package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<Student> student = new ArrayList<Student>();
        Student student1 = new Student("Michael", 19, 80);
        Student student2 = new Student("James", 19, 65);
        Student student3 = new Student("David", 18, 64);
        Student student4 = new Student("Robert", 20, 77);
        Student student5 = new Student("Kevin", 22, 88);
        Student student6 = new Student("Mark", 23, 91);
        Student student7 = new Student("John", 21, 77);

        student.add(student1);
        student.add(student2);
        student.add(student3);
        student.add(student4);
        student.add(student5);
        student.add(student6);
        student.add(student7);


        Collections.sort(student, new Student());

        for (Student st : student) {
            System.out.println(st.name + " , " + st.age + " , " + st.score);

        }


    }
}
