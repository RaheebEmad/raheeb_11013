package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        ArrayList<House> houses = new ArrayList<House>();


        House house1 = new House("Kazan", " Khusaina Mavlyutova street 13 ", 3500000);
        House house2 = new House("Moscow", " Mytnaya street 34a ", 10216000);
        House house3 = new House("Petersburg", " Nevsky ave street 65 ", 9300000);
        House house4 = new House("Nizhny Novgorod", " Minina street 92 ", 6650000);

        houses.add(house1);
        houses.add(house2);
        houses.add(house3);
        houses.add(house4);

        Collections.sort(houses);
        for (House house : houses) {
            System.out.println(house.city + "," + house.street + "," + house.price);

        }


    }
}