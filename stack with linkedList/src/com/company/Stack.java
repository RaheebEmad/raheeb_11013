package com.company;

public class Stack {

    Node top;
    int size;


    public int size() {

        return size;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        newNode.next = top;
        top = newNode;
        size++;
    }


    public int pop() {
        if (isEmpty()) {
            System.out.println("the stack is empty");
        }
        int result = top.data;
        top = top.next;
        size--;
        return result;
    }

    public int peek() {
        if (isEmpty()) {
            System.out.println("the stack is empty");
        }
        return top.data;
    }

    public void displayStack() {
        Node current = top;
        while (current != null) {
            System.out.print(current.data + " ");
            current = current.next;
        }
    }


}
